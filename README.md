# Hide Facebook Comments

## COMMENTS CONSIDERED HARMFUL

We all know the phrase "don't read the comments," and yet we do.
Squash these pesky varmints once and for all with this helpful UserScript.

Tested with GreaseMonkey Firefox.

Alpha software, use at your own risk.